import { AsyncStorage } from 'react-native';

const ReduxPersist = {
  active: true,
  reducerVersion: '0.1' ,
  storeConfig: {
    key: 'primary',
    storage: AsyncStorage,
    whitelist: ['user'],
  }
};

export default ReduxPersist;
