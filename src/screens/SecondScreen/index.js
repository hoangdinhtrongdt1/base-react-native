import React, { PureComponent } from 'react';
import { View } from 'react-native';
import styles from './SecondScreen.style';

class SecondScreen extends PureComponent {
  render() {
    return (
      <View style={styles.container}/>
    );
  }
}

export default SecondScreen;