import { StyleSheet } from 'react-native';
import Colors from '../../themes/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.green
  },
  Text: {
    fontSize: 20,
    color: Colors.white,
    marginVertical: 20,
  }
});
