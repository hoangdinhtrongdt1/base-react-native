import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import styles from './HomeScreen.style';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/UserRedux';
import APi from '../../api';

class HomeScreen extends PureComponent {
  _navigation = () => {
    this.props.navigation.navigate('SecondScreen');
  }

  _onSaga = () => {
    this.props.login();
  }

  componentDidMount() {
    const param = {

    }
    APi.user.login(param)
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.Text} onPress={this._navigation}>Next Screen</Text>
        <Text style={styles.Text} onPress={this._onSaga}>Saga</Text>
        <Text style={styles.Text}>{this.props.token}</Text>
      </View>
    );
  }
}

export default connect(state =>({
  token: state.user.token
}), { login })(HomeScreen);