import { call, put } from 'redux-saga/effects';
import { loginSuccess } from '../redux/actions/UserRedux';

export default function* login() {
  yield put(loginSuccess({ token: 'loginSuccess' }));
}