import { takeLatest, all } from 'redux-saga/effects';
import Types from '../redux/types';
import UserSaga from './UserSaga';

export default function* root() {
  yield all([
    takeLatest(Types.LOGIN_REQUEST, UserSaga)
  ]);
};