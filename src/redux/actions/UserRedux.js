import Types from '../types';

const login = () => ({
  type: Types.LOGIN_REQUEST
});

const loginSuccess = payload => ({
  type: Types.LOGIN_SUCCESS,
  payload
});

export {
  login,
  loginSuccess
} 