import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers';
import rootSaga from '../../sagas';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist'
import ReduxPersist from '../../config/ReduxPersist';
import Rehydration from '../../services/Rehydration';

const persistedReducer = persistReducer(ReduxPersist.storeConfig, rootReducer)
const sagaMiddleware = createSagaMiddleware();
const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
Rehydration.updateReducers(store);
sagaMiddleware.run(rootSaga);

const persistor = persistStore(store)
export { store, persistor};