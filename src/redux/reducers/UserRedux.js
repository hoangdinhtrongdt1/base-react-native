import Types from '../types';

const initState = {
  token: '12345678'
}

export default function UserRedux(state = initState, { type, payload }) {
  switch (type) {
    case Types.LOGIN_SUCCESS: return { ...state, ...payload };
    default: return state;
  }
}