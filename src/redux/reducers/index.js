import { combineReducers } from 'redux';
import user from './UserRedux';

const reducers = combineReducers({
  user
});

export default reducers;