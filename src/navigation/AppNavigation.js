import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import SecondScreen from '../screens/SecondScreen';

const AppStack = createStackNavigator(
  {
    HomeScreen: { screen: HomeScreen },
    SecondScreen: { screen: SecondScreen }
  },
  {
    headerMode: 'none',
    initialRouteName: 'HomeScreen'
  }
);
export default createAppContainer(AppStack);