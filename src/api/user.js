/* eslint-disable no-caller,no-restricted-properties,no-prototype-builtins */
import client from './configAPI';

export default {
  login(params, type) {
    const url = `/login/${type}/`;
    const mParams = {
      email: params.email,
      password: params.password,
    };
    // noinspection JSAnnotator
    return client.post(url, mParams, {
      apiName: arguments.callee.name
    });
  },
};
