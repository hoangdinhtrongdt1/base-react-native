import React, {PureComponent} from 'react';
import {Provider} from 'react-redux';
import AppNavigation from './src/navigation/AppNavigation';
import { store, persistor } from './src/redux/store';
import NavigationService from './src/services/NavigationService';
import { PersistGate } from 'redux-persist/integration/react'

export default class App extends PureComponent {
  render() {
    return (
      <Provider store = {store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppNavigation 
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </PersistGate>
      </Provider>
    );
  }
}
